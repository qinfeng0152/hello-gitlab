package fr;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by fengqin on 15/05/2017.
 */
public class HelloTest {

    @Test
    public void testSayHi(){

        final String result = "Hi";
        Hello hello = new Hello();
        assertEquals(result, hello.sayHi());


    }
}
